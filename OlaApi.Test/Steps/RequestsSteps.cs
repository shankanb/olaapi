﻿using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using OlaApi.Test.Mocks;
using System;
using System.Collections.Generic;
using System.Text;
using TechTalk.SpecFlow;
using Xunit;

namespace OlaApi.Test.Steps
{
    [Binding]
    [Scope(Feature = "Requests")]
    class RequestsSteps : BaseSteps
    {
        public RequestsSteps(ScenarioContext scenario ,WebApplicationFactory<TestStartup> factory):base(
            scenario,   
            factory.WithWebHostBuilder(builder => {
                builder.ConfigureServices(services =>
                {
                    services.AddScoped(services => RequestMock.MockRequestsRepository.Object);
                });
            }))
        {
            RequestMock.MockUpImplementation();
        }
    }
}
