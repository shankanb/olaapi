﻿using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using OlaApi.Test.Mocks;
using System;
using System.Collections.Generic;
using System.Text;
using TechTalk.SpecFlow;

namespace OlaApi.Test.Steps
{
    [Binding]
    [Scope(Feature = "Rides")]
    class RidesSteps : BaseSteps
    {
        public RidesSteps(ScenarioContext scenario, WebApplicationFactory<TestStartup> factory) : base(
            scenario,
               factory.WithWebHostBuilder(builder => builder.ConfigureServices(services =>
               {
                   services.AddScoped(service => RideMock.MockRidesRepository.Object);
               }))
            )
        {
            RideMock.MockUpImplentation();
        }
    }
}
