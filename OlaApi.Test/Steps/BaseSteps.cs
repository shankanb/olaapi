﻿using Microsoft.AspNetCore.Mvc.Testing;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using TechTalk.SpecFlow;
using Xunit;

namespace OlaApi.Test.Steps
{
    class BaseSteps : IClassFixture<WebApplicationFactory<TestStartup>>
    {
        private ScenarioContext _scenario;
        private WebApplicationFactory<TestStartup> _factory;
        private HttpClient _client;
        //private string _authToken;
        public BaseSteps(ScenarioContext scenario, WebApplicationFactory<TestStartup> factory)
        {
            _scenario = scenario;
            _factory = factory;
        }

        [Given(@"a client")]
        public void GivenAClient()
        {
            _client = _factory.CreateClient(new WebApplicationFactoryClientOptions() { 
                BaseAddress = new Uri("http://localhost/")
            });
        }

        [Given(@"HTTP request body content is '(.*)'")]
        public void GivenHTTPRequestBodyContentIs(string p0)
        {
            _scenario.Add("Request Body Content",p0);
        }


        [When(@"a POST request is made to '(.*)'")]
        public void WhenAPOSTRequestIsMadeTo(string p0)
        {
            // THIS COMMENTED SECTION IS TO TEST AUTHENTICATION OF USER
            //HttpContent payload = new StringContent("{\"email\":\"josh@email.com\",\"password\":\"1234567890\"}");
            //payload.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            //_authToken = _client.PostAsync("/users/authenticate",payload)
            //    .GetAwaiter()
            //    .GetResult()
            //    .Content
            //    .ReadAsStringAsync()
            //    .GetAwaiter()
            //    .GetResult();
            //payload = new StringContent(_requestData);
            //payload.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            //_client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _authToken);


            var stringContent = new StringContent(_scenario.Get<string>("Request Body Content"), Encoding.UTF8, "application/json");
            var response = _client.PostAsync(p0, stringContent)
                .GetAwaiter()
                .GetResult();
            _scenario.Add("Response Status Code", response.StatusCode);
            _scenario.Add("Response", response.Content.ReadAsStringAsync().GetAwaiter().GetResult());
        }

        [Then(@"the status code should be '(.*)'")]
        public void ThenTheStatusCodeShouldBe(int p0)
        {
            var actualCode = _scenario.Get<HttpStatusCode>("Response Status Code");
            Assert.Equal((HttpStatusCode)p0, actualCode);
        }

        [When(@"a GET request is made to '(.*)'")]
        public void WhenAGETRequestIsMadeTo(string p0)
        {
            var response = _client.GetAsync(p0)
                .GetAwaiter()
                .GetResult();
            _scenario.Add("Response Status Code", response.StatusCode);
            _scenario.Add("Response", response.Content.ReadAsStringAsync().GetAwaiter().GetResult());
        }

        [When(@"a PATCH request is made to '(.*)'")]
        public void WhenAPATCHRequestIsMadeTo(string p0)
        {
            var stringContent = new StringContent("{'Status':'Completed'}", Encoding.UTF8, "application/json");
            var response = _client.PatchAsync(p0, stringContent)
                .GetAwaiter()
                .GetResult();
            _scenario.Add("Response Status Code", response.StatusCode);
            _scenario.Add("Response", response.Content.ReadAsStringAsync().GetAwaiter().GetResult());
        }
        [Then(@"the response should look like '(.*)'")]
        public void ThenTheResponseShouldLookLike(string p0)
        {
            var actual = _scenario.Get<string>("Response");
            Assert.Equal(p0, actual);
        }

    }
}
