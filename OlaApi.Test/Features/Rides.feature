﻿Feature: Rides
	In order to manage rides in the Ola App, a service is needed for this purpose

Scenario: View user's past rides
	Given a client
	When a GET request is made to '/users/1/rides'
	Then the status code should be '200'
	And the response should look like '[{"id":1,"requestId":1,"driverId":1,"statusId":1},{"id":1,"requestId":2,"driverId":3,"statusId":2}]'

Scenario: Create a new ride
	Given a client
	And HTTP request body content is '{"RequestId":100}'
	When a POST request is made to '/drivers/1/rides/'
	Then the status code should be '201'
	And the response should look like '1'

Scenario: Complete a ride
	Given a client
	When a PATCH request is made to '/drivers/1/rides/100/completed'
	Then the status code should be '200'
	And the response should look like 'Ride 100 completed'