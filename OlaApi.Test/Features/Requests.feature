﻿Feature: Requests
	In order to manage ride requests, a service is needed to fulfill this purpose

Scenario: Make a ride request
	Given a client
	And HTTP request body content is '{"source":"HSR Layout","destination":"Whitefield"}'
	When a POST request is made to '/users/1/requests'
	Then the status code should be '201'
	And the response should look like '{"id":1,"userId":1,"source":"HSR Layout","destination":"Whitefield"}'

Scenario: Get all requests based on driver's current location
	Given a client
	When a GET request is made to '/drivers/1/requests?currentLocation=HSR'
	Then the status code should be '200'
	And the response should look like '[{"id":1,"userId":1,"source":"AAA","destination":"BBB"},{"id":2,"userId":1,"source":"CCC","destination":"DDD"}]'

Scenario: View user's past requests
	Given a client
	When a GET request is made to '/users/1/requests'
	Then the status code should be '200'
	And the response should look like '[{"id":1,"userId":1,"source":"AAA","destination":"BBB"},{"id":2,"userId":1,"source":"CCC","destination":"DDD"}]'