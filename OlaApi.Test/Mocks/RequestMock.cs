﻿using Moq;
using OlaApi.Repositories.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OlaApi.Test.Mocks
{
    class RequestMock
    {
        public static readonly Mock<IRequestsRepository> MockRequestsRepository = new Mock<IRequestsRepository>();
        public static List<Models.RideRequests> requests = new List<Models.RideRequests>()
        {
            new Models.RideRequests()
            {
                Id=1,
                UserId=1,
                source="AAA",
                destination="BBB"
            },
            new Models.RideRequests()
            {
                Id=2,
                UserId=1,
                source="CCC",
                destination="DDD"
            }
        };

        public static void MockUpImplementation()
        {
            MockRequestsRepository.Setup(repo => repo.Create(It.IsAny<Models.RideRequests>())).Returns((Models.RideRequests rideRequests) =>
            {
                return 1;
            });

            MockRequestsRepository.Setup(repo => repo.Get(It.IsAny<int>())).Returns((int id) =>
            {
                return requests.Where(req => req.UserId == id);
            });
            MockRequestsRepository.Setup(repo => repo.Check(It.IsAny<int>())).Returns((int id) =>
            {
                return 1;
            });
            MockRequestsRepository.Setup(repo => repo.Get()).Returns(() =>
            {
                return requests;
            });
        }
    }
}
