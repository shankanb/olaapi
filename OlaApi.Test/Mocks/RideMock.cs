﻿using Moq;
using OlaApi.Models;
using OlaApi.Repositories.Rides;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OlaApi.Test.Mocks
{
    class RideMock
    {
        public static readonly Mock<IRidesRepository> MockRidesRepository = new Mock<IRidesRepository>();
        public static List<Rides> rides = new List<Rides>()
        {
            new Rides()
            {
                id=1,
                RequestId=1,
                DriverId=1,
                StatusId=Enums.Status.Pending
            },
            new Rides()
            {
                id=1,
                RequestId=2,
                DriverId=3,
                StatusId=Enums.Status.Completed
            },
            new Rides()
            {
                id=2,
                RequestId=5,
                DriverId=32,
                StatusId=Enums.Status.Pending
            }
        };
        public static void MockUpImplentation()
        {
            MockRidesRepository.Setup(repo => repo.Get(It.IsAny<int>())).Returns((int id) =>
            {
                return rides.Where(ride => ride.id == id);
            });
            MockRidesRepository.Setup(repo => repo.Create(It.IsAny<Rides>())).Returns(() =>
            {
                return 1;
            });
            MockRidesRepository.Setup(repo => repo.Update(It.IsAny<int>())).Returns(() =>
            {
                return 1;
            });
        }
    }
}
