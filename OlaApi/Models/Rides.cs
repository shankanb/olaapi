﻿using OlaApi.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OlaApi.Models
{
    public class Rides
    {
        public int id { get; set; }
        public int RequestId { get; set; }
        public int DriverId { get; set; }
        public Status StatusId { get; set; }
    }
}
