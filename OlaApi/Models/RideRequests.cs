﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OlaApi.Models
{
    public class RideRequests
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string source { get; set; }
        public string destination { get; set; }

    }
}
