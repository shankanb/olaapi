﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OlaApi.Models.Requests
{
    public class UserLogin
    {
        public string email { get; set; }
        public string password { get; set; }
    }
}
