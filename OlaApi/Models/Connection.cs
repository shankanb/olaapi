﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OlaApi.Models
{
    public class Connection
    {
        public string DBConnection { get; set; }
    }
}
