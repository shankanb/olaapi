﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OlaApi.Enums
{
    public enum Status
    {
        Pending = 1,
        Completed = 2
    }
}
