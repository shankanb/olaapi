﻿using OlaApi.Enums;
using OlaApi.Repositories.Rides;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OlaApi.Services.Rides
{
    public class RidesService : IRidesService
    {
        public RidesService(IRidesRepository ridesRepository)
        {
            _ridesRepository = ridesRepository;
        }

        private IRidesRepository _ridesRepository;

        public IEnumerable<Models.Rides> Get(int userId)
        {
            return _ridesRepository.Get(userId);
        }

        public int Create(int driverId ,Models.Requests.Rides ridesRequestModel)
        {
            var rides = new Models.Rides
            {
                DriverId = driverId,
                RequestId = ridesRequestModel.RequestId,
                StatusId = Status.Pending
            };
            return _ridesRepository.Create(rides);
        }

        public int Update(int id)
        {
            return _ridesRepository.Update(id);
        }
    }
}
