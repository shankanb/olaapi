﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OlaApi.Services.Rides
{
    public interface IRidesService
    {
        public IEnumerable<Models.Rides> Get(int userId);
        public int Create(int driverId, Models.Requests.Rides ridesRequestModel);
        public int Update(int id);
    }
}
