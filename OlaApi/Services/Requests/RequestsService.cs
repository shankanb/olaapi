﻿using OlaApi.Models;
using OlaApi.Repositories.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OlaApi.Services.Requests
{
    public class RequestsService : IRequestsService
    {
        private IRequestsRepository _requestsRepository;
        public RequestsService(IRequestsRepository requestsRepository)
        {
            _requestsRepository = requestsRepository;
        }
        public int Create(int userId, RideRequests request)
        {
            int noOfRows = _requestsRepository.Check(userId);
            if (noOfRows > 0)
            {
                request.UserId = userId;
                return _requestsRepository.Create(request);
            }
            return 0;
        }

        public IEnumerable<RideRequests> Get(int userId)
        {
            return _requestsRepository.Get(userId);
        }

        public IEnumerable<RideRequests> Get()
        {
            return _requestsRepository.Get();
        }
    }
}
