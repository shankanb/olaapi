﻿using OlaApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OlaApi.Services.Requests
{
    public interface IRequestsService
    {
        public int Create(int userId, RideRequests request);
        public IEnumerable<RideRequests> Get(int userId);
        public IEnumerable<RideRequests> Get();
    }
}
