﻿using OlaApi.Models.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OlaApi.Services.Users
{
    public interface IUserService
    {
        public string Authenticate(UserLogin usersLogin);
    }
}
