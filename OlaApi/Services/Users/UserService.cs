﻿using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using OlaApi.Models;
using OlaApi.Models.Requests;
using OlaApi.Repositories.Users;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace OlaApi.Services.Users
{
    public class UserService : IUserService
    {
        private IUserRepository _userRepository;
        public UserService(IUserRepository userRepository, IOptions<JwtToken> options)
        {
            _userRepository = userRepository;
            _options = options;
        }

        private IOptions<JwtToken> _options;

        public string Authenticate(UserLogin usersLogin)
        {
            Models.Users user = _userRepository.GetByEmail(usersLogin.email);
            string token = "";
            if (usersLogin.password == user.password)
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var symKey = Encoding.ASCII.GetBytes(_options.Value.Key);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[] {
                        new Claim(ClaimTypes.Name, user.id.ToString())
                    }),
                    Expires = DateTime.UtcNow.AddDays(7),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(symKey), SecurityAlgorithms.HmacSha256Signature)
                };
                token = tokenHandler.WriteToken(tokenHandler.CreateToken(tokenDescriptor));
            }
            return token;
        }
    }
}
