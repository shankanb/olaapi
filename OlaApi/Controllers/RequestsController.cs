﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OlaApi.Models;
using OlaApi.Services.Requests;

namespace OlaApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class RequestsController : ControllerBase
    {
        private IRequestsService _requestsService;

        public RequestsController(IRequestsService service)
        {
            _requestsService = service;
        }
        [HttpGet]
        [Route("~/users/{UserId}/requests")]
        //[Authorize]
        public IActionResult GetRequestsForUsers(int UserId)
        {
            var result = _requestsService.Get(UserId);
            return Ok(result);
        }

        [HttpGet]
        [Route("~/drivers/{DriverId}/requests")]
        public IActionResult GetRequestsForDrivers(int DriverId, string currentLocation)
        {
            var result = _requestsService.Get();
            return Ok(result);
        }


        [HttpPost]
        [Route("~/users/{UserId}/requests")]
        //[Authorize]
        public IActionResult Post(int userId, [FromBody]RideRequests body)
        {
            var result = _requestsService.Create(userId, body);
            if (result == 0)
            {
                return BadRequest("Previous request is pending");
            }
            body.Id = result;
            body.UserId = userId;
            return Created($"/users/{userId}/requests",body);
        }
    }
}