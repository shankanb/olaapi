﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OlaApi.Models.Requests;
using OlaApi.Services.Users;

namespace OlaApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private IUserService _userService;
        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [AllowAnonymous]
        [HttpPost, Route("authenticate")]
        public IActionResult Authenticate([FromBody] UserLogin userLogin)
        {
            var token = _userService.Authenticate(userLogin);
            if (token.Length <= 0)
            {
                return BadRequest(new { message = "Username or password is incorrect" });
            }
            return Ok(token);
        }

        [HttpGet, Route("test")]
        [Authorize]
        public IActionResult Test()
        {
            return Ok("OK");
        }
    }
}