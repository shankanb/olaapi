﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OlaApi.Services.Rides;

namespace OlaApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class RidesController : ControllerBase
    {
        public RidesController(IRidesService ridesService)
        {
            _ridesService = ridesService;
        }

        private IRidesService _ridesService;

        [HttpGet]
        [Route("~/users/{userId}/rides")]
        //[Authorize]
        public IActionResult Get(int UserId)
        {
            var result = _ridesService.Get(UserId);
            return Ok(result);
        }

        [HttpPost]
        [Route("~/drivers/{DriverId}/rides")]
        public IActionResult Post(int DriverId, [FromBody]Models.Requests.Rides body)
        {
            var result = _ridesService.Create(DriverId, body);
            return Created($"/drivers/{DriverId}/rides", result);
        }

        [HttpPatch]
        [Route("~/drivers/{driverId}/rides/{id}/completed")]
        public IActionResult UpdateStatus(int driverId, int id)
        {
            _ridesService.Update(id);
            return Ok($"Ride {id} completed");
        }
    }
}