﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OlaApi.Repositories.Users
{
    public interface IUserRepository
    {
        public Models.Users GetByEmail(string email);
    }
}
