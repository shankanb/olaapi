﻿using Microsoft.Extensions.Options;
using OlaApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OlaApi.Repositories.Users
{
    public class UserRepository : BaseRepository<Models.Users>, IUserRepository
    {
        public UserRepository(IOptions<Connection> options):base(options.Value)
        {

        }
        public Models.Users GetByEmail(string email)
        {
            string query = "SELECT * FROM Users WHERE Email = @Email;";
            return GetByEmail(query,new { Email = email });
        }
    }
}
