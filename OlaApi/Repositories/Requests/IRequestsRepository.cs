﻿using OlaApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OlaApi.Repositories.Requests
{
    public interface IRequestsRepository
    {
        public int Create(RideRequests request);
        public int Check(int userId);
        public IEnumerable<RideRequests> Get(int userId);
        public IEnumerable<RideRequests> Get();
    }
}
