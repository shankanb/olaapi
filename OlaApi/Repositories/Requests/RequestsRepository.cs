﻿using Microsoft.Extensions.Options;
using OlaApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OlaApi.Repositories.Requests
{
    public class RequestsRepository : BaseRepository<RideRequests>, IRequestsRepository
    {
        public RequestsRepository(IOptions<Connection> options):base(options.Value)
        {

        }
        public int Check(int userId)
        {
            string query = @"SELECT
                              COUNT(UserId)
                            FROM Requests
                            WHERE UserId = @Userid;";
            return CheckIfRequestExists(query, new { UserId = userId });

        }

        public int Create(RideRequests request)
        {
            string query = @"INSERT INTO Requests (UserId, Source, Destination)
                                OUTPUT inserted.Id
                                VALUES (@UserID, @Source, @Destination)";
            return Create(query, new { UserId = request.UserId, Source = request.source, Destination = request.destination });
        }

        public IEnumerable<RideRequests> Get(int userId)
        {
            string query = @"SELECT
                              *
                            FROM Requests
                            WHERE UserId = @UserId;";
            return Get(query, new { UserId = userId });
        }

        public IEnumerable<RideRequests> Get()
        {
            string query = @"SELECT
                              *
                            FROM Requests;";
            return Get(query, null);
        }
    }
}
