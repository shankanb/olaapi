﻿using Dapper;
using OlaApi.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace OlaApi.Repositories
{
    public class BaseRepository<T>
    {
        private Connection _connection;

        public BaseRepository(Connection connection)
        {
            _connection = connection;
        }

        public T GetByEmail(string query, object param)
        {
            using(SqlConnection connection = new SqlConnection(_connection.DBConnection))
            {
                return connection.Query<T>(query, param).FirstOrDefault();
            }
        }

        public int Create(string query, object param)
        {
            using (SqlConnection connection = new SqlConnection(_connection.DBConnection))
            {
                return connection.Query<int>(query, param).FirstOrDefault();
            }
        }

        public int CheckIfRequestExists(string query, object param)
        {
            using (SqlConnection connection = new SqlConnection(_connection.DBConnection))
            {
                return connection.Query<int>(query, param).FirstOrDefault();
            }
        }

        public IEnumerable<T> Get(string query, object param)
        {
            using (SqlConnection connection = new SqlConnection(_connection.DBConnection))
            {
                return connection.Query<T>(query, param);
            }
        }

        public int Update(string query, object param)
        {
            using (SqlConnection connection = new SqlConnection(_connection.DBConnection))
            {
                return connection.Query<int>(query, param).FirstOrDefault();
            }
        }
    }
}
