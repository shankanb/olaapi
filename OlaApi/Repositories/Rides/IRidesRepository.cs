﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OlaApi.Repositories.Rides
{
    public interface IRidesRepository
    {
        public IEnumerable<Models.Rides> Get(int userId);

        public int Create(Models.Rides rides);
        public int Update(int id);
    }
}
