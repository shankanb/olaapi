﻿using Microsoft.Extensions.Options;
using OlaApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OlaApi.Repositories.Rides
{
    public class RidesRepository : BaseRepository<Models.Rides>,  IRidesRepository
    {
        public RidesRepository(IOptions<Connection> options):base(options.Value)
        {

        }

        public int Create(Models.Rides rides)
        {
            string query = @"INSERT INTO Rides (RequestId, DriverId, StatusId)
                                OUTPUT INSERTED.Id
                                  VALUES (@RequestId, @DriverId, @StatusId);";
            return Create(query, new { RequestId = rides.RequestId, DriverId = rides.DriverId, StatusId = rides.StatusId });
        }

        public IEnumerable<Models.Rides> Get(int userId)
        {
            string query = @"SELECT
                              t1.*
                            FROM Rides t1
                            INNER JOIN Requests t2
                              ON t1.RequestId = t2.Id
                            WHERE t2.UserId = @UserId;";
            return Get(query, new { UserId = userId });
        }

        public int Update(int id)
        {
            string query = @"UPDATE Rides
                                SET StatusId = 2
                                WHERE id = @Id;";
            return Update(query, new { Id=id });
        }
    }
}
