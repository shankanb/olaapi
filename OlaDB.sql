CREATE DATABASE OlaDB

USE OlaDB

CREATE TABLE Users (
  Id int PRIMARY KEY IDENTITY,
  Name varchar(100),
  Email varchar(100),
  Password varchar(100)
)
CREATE TABLE Drivers (
  Id int PRIMARY KEY IDENTITY,
  Name varchar(100),
  Email varchar(100),
  Password varchar(100),
  CarNumber varchar(100)
)
CREATE TABLE Requests (
  Id int PRIMARY KEY IDENTITY,
  UserId int REFERENCES Users (Id),
  Source varchar(100),
  Destination varchar(100)
)
CREATE TABLE Status (
  Id int PRIMARY KEY IDENTITY,
  StatusName varchar(100)
)
CREATE TABLE Rides (
  Id int PRIMARY KEY IDENTITY,
  RequestId int REFERENCES Requests (Id),
  DriverId int REFERENCES Drivers (Id),
  StatusId int REFERENCES Status (id)
)